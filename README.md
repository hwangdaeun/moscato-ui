# MOSCATO UI

## Requirements

* Back-end
    * Python 3.5.0+
    * Flask 0.17.0+

* Front-end
    * webpack 2+
    * React 16.0+
    * Redux 3.0+
    * Strophe.js (Optional : For XMPP Communication)

## Installation

```bash
npm install
pip install -r requirements.txt
```

### Setting up

* Back-end
    * Modify properly `settings.py.template` file and save it to `settings.py`
* Front-end
    * Modify properly `.env.template` file and save it to `.env`

## How to develop

* Back-end Development
    * Database
        * Initiation : `flask db init`
        * Migration : `flask db migrate`
    * Execution
        * Just run 
        * Just run `FLASK_APP=dashboard.py flask run`

* Front-end Development
Execute `npm run watch` to run webpack.


