from flask import Flask
from flask_sqlalchemy import SQLAlchemy
# from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

# app.config['SQLALCHEMY_DATABASE_URI'] = "postgres:///hiarc_dashboard"
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from routes.main import main

from routes.api.chef import chef_api
from routes.api.elastic_search import elastic_search_api
from routes.api.mesos import mesos_api
from routes.api.ejabberd import ejabberd_api
from routes.api.spark import spark_api

app.register_blueprint(main)

app.register_blueprint(chef_api)
app.register_blueprint(elastic_search_api)
app.register_blueprint(mesos_api)
app.register_blueprint(ejabberd_api)
app.register_blueprint(spark_api)


if __name__ == '__main__':
    app.run()
