from models import Base
from sqlalchemy import Column, Integer, String


class Student(Base):
    __tablename__ = 'students'
    id        = Column(Integer, primary_key=True)
    name      = Column(String(50))
    faculty   = Column(String(50))
    phone     = Column(String(50))
    email     = Column(String(120), unique=True)
    portfolio = Column(String(200))
    comment   = Column(String(1000))
    
    
    def __init__(self, apply_form):
        self.name    = apply_form['name']
        self.faculty = apply_form['faculty']
        self.phone = apply_form['phone']
        self.email   = apply_form['email']
        self.portfolio = apply_form['portfolio']
        self.comment = apply_form['comment']                                              
