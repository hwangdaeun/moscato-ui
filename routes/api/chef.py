from flask import Blueprint, jsonify
import requests

from settings import CHEF_URL

chef_api = Blueprint('chef_api', __name__)

@chef_api.route('/api/chef')
def chef_home():
    return jsonify(message=CHEF_URL)