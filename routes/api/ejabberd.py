from flask import Blueprint, jsonify
import requests

from settings import EJABBERD_URL

ejabberd_api = Blueprint('ejabberd_api', __name__)

@ejabberd_api.route('/api/chef')
def ejabberd_home():
    return jsonify(message=EJABBERD_URL)