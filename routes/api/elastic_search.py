from flask import Blueprint, jsonify
import requests

from settings import ELASTIC_URL

elastic_search_api = Blueprint('elastic_search_api', __name__)

@elastic_search_api.route('/api/elastic_search')
def elastic_home():
    return jsonify(message=ELASTIC_URL)