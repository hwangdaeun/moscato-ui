from flask import Blueprint, jsonify
import requests

from settings import MESOS_URL

mesos_api = Blueprint('mesos_api', __name__)

@mesos_api.route('/api/mesos')
def mesos_home():
    return jsonify(message=MESOS_URL)