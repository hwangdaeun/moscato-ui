from flask import Blueprint, jsonify
import requests

from settings import SPARK_URL

spark_api = Blueprint('SPARK_api', __name__)

@spark_api.route('/api/spark')
def spark_home():
    return jsonify(message=SPARK_URL)