import * as React from 'react'
import {BrowserRouter} from 'react-router-dom'

import {Header, Section, Footer} from './components'

var styles = require('./app.sass')

class App extends React.Component {
  render() {
      return (
        <div className="app">
          <Header />
          <Section />
          <Footer />
        </div>
      );
  }
}

export default App;