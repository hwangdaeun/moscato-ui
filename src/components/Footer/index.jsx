import * as React from 'react';

export class Footer extends React.Component {
    render() {
        return (
            <div className="Footer">
                <p>Copyright By APL LAB</p>
            </div>
        );
    }
}