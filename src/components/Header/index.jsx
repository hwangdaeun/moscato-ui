import * as React from 'react';
import {NavLink,Link} from 'react-router-dom'

export class Header extends React.Component {
    render() {
        return (
            <div className="Header">
                <p>Hell-o-world</p>
                <ul role="nav">
                    <li>
                       <NavLink to="/">Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/mesos">Mesos</NavLink>
                    </li>
                    <li>
                        <NavLink to="/ejabberd">Ejabberd</NavLink>
                    </li>
                    <li>
                        <NavLink to="/elastic">Elastic</NavLink>
                    </li>
                </ul>
            </div>
        );
    }
}