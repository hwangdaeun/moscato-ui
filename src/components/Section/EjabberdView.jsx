import * as React from 'react';
import {EjabberdAPI} from '../common/APICall'

export class EjabberdView extends React.Component {
    constructor(props) {
        super(props)
        this.state = { description: "" }
    }

    fetchData() {
        EjabberdAPI.post('/api/connected_users_number', {})
            .then((res) => { this.state.description = res.toString(); console.log(res); })
    }


    render() {
        return (
            <div>
                <p>Hell-o-Ejabberd</p>
                <p>{this.state.description}</p>
                <button onClick={this.fetchData}></button>
            </div>
        )
    }
}