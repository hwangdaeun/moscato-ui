import * as React from 'react';
import {MesosAPI} from '../common/APICall'

export class MesosView extends React.Component {
    constructor(props) {
        super(props)
        this.state = { description: '' }
    }


    fetchData() {
        MesosAPI.get('/roles.json')
            .then((res) => { this.state.description = res.toString(); console.log(res); })
    }

    render() {
        return (
            <div>
                <p>Hell-o-Mesos</p>
                <p>{this.state.description}</p>
                <button onClick={this.fetchData}></button>
            </div>
        )
    }
}