import * as React from 'react';
import {Route,Router,hashHistory} from 'react-router-dom'

import {HomeView} from './HomeView'
import {MesosView} from './MesosView'
import {SparkView} from './SparkView'
import {EjabberdView} from './EjabberdView'
import {ElasticSearchView} from './ElasticSearchView'


export class Section extends React.Component {
    render() {
        return (
            <div className="Section">  
                <Route exact path="/" component={HomeView} />
                <Route path="/mesos" component={MesosView} />
                <Route path="/ejabberd" component={EjabberdView} />
                <Route path="/spark" component={SparkView} />
                <Route path="/elastic" component={ElasticSearchView} />
            </div>
        );
    }
}
