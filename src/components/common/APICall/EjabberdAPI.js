import axios from 'axios'

export const EjabberdAPI = axios.create({
    baseURL: process.env.API_SERVER + '/api/ejabberd',
    timeout: 10000,
    withCredentials: true,
    transformRequest: [(data) => JSON.stringify(data)],
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
});