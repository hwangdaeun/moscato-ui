import axios from 'axios'

export const ElasticSearchAPI = axios.create({
    baseURL: process.env.API_SERVER + '/api/elastic_search',
    timeout: 10000,
    withCredentials: true,
    transformRequest: [(data) => JSON.stringify(data)],
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
});