import axios from 'axios'

export const MesosAPI = axios.create({
    baseURL: process.env.API_SERVER + '/api/mesos',
    timeout: 10000,
    withCredentials: true,
    transformRequest: [(data) => JSON.stringify(data)],
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
});