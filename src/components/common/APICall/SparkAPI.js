import axios from 'axios'

export const SparkAPI = axios.create({
    baseURL: process.env.API_SERVER + '/api/spark',
    timeout: 10000,
    withCredentials: true,
    transformRequest: [(data) => JSON.stringify(data)],
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
});