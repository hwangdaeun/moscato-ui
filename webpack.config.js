var webpack = require('webpack');
var path = require('path');

var env = process.env.MIX_ENV || 'dev';
var isProduction = (env === 'prod');

const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: __dirname + "/src/index.jsx",
    output: {
        path: __dirname + "/static/js",
    	filename: "app.js"
    },
    
    resolve: {
	    extensions: [".js", ".jsx", ".sass"]
    },
    
    module: {
        loaders: [
            { test: /\.jsx?$/, loader: "babel-loader", exclude: /node_modules/},
            { test: /\.sass$/, loader: "style-loader!css-loader!sass-loader"}
        ]
	
    //	resolve: {
    //	    modulesDirectories: [ "node_modules", __dirname + "/web/static/js" ]
    //	}
    },

    plugins: [
        new Dotenv()
    ]
}
